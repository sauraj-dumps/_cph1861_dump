## full_oppo6771_17065-user 9 PPR1.180610.011 eng.root.20200509.060522 release-keys
- Manufacturer: oppo
- Platform: mt6771
- Codename: CPH1861
- Brand: 
- Flavor: full_oppo6771_17065-user
- Release Version: 9
- Id: PPR1.180610.011
- Incremental: 1588975780
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: 
- OTA version: 
- Branch: full_oppo6771_17065-user-9-PPR1.180610.011-eng.root.20200509.060522-release-keys
- Repo: _cph1861_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
